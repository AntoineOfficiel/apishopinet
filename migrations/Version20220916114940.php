<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220916114940 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE stock (id INT AUTO_INCREMENT NOT NULL, quantity DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE article ADD stock_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E66DCD6110 FOREIGN KEY (stock_id) REFERENCES stock (id)');
        $this->addSql('CREATE INDEX IDX_23A0E66DCD6110 ON article (stock_id)');
        $this->addSql('ALTER TABLE warehouse ADD stock_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE warehouse ADD CONSTRAINT FK_ECB38BFCDCD6110 FOREIGN KEY (stock_id) REFERENCES stock (id)');
        $this->addSql('CREATE INDEX IDX_ECB38BFCDCD6110 ON warehouse (stock_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E66DCD6110');
        $this->addSql('ALTER TABLE warehouse DROP FOREIGN KEY FK_ECB38BFCDCD6110');
        $this->addSql('DROP TABLE stock');
        $this->addSql('DROP INDEX IDX_23A0E66DCD6110 ON article');
        $this->addSql('ALTER TABLE article DROP stock_id');
        $this->addSql('DROP INDEX IDX_ECB38BFCDCD6110 ON warehouse');
        $this->addSql('ALTER TABLE warehouse DROP stock_id');
    }
}
