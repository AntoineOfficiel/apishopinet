<?php

namespace App\DataFixtures;

use App\Entity\Article;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ArticleFixture extends Fixture
{

    static $categories = [0 => "t-shirst", 1 => "pull", 2 =>"jean"];
    static $pictures = [0 => ""];
    private ObjectManager $manager;



    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;

        $this->generateArticle(5);

        $this->manager->flush();
    }

    private function generateArticle(int $number)
    {
        $i = 0;
        while($i < $number)
        {
            $article = new Article();

            $article->setName("habit{$i}")
                   ->setPrice(rand(0, 100))
                   ->setDescription("Un habit de dingue !!!!")
                   ->setIsPublised(($i % 2 == 0) ? true : false)
                   ;

            $i++;

            $this->manager->persist($article);
        }
    }
}
